#!/bin/sh
cd source && git pull
cd ..
docker-compose run --rm artisan cache:clear
docker-compose run --rm artisan optimize
docker-compose run --rm artisan migrate
docker-compose run --rm node run init
docker-compose run --rm node run prod


## Ports


| Stack | Port |
|-------------- | -------------- |
| **nginx** | 8080 |
| **mysql** | 3306 |
| **php** | 9000 |

## Penggunaan

1. Buat file .env:

   ```sh
   cp .env.example .env
   ```

2. Simpan project Laravel di folder  **source**.

3. Build docker:

   ```sh
   docker-compose up -d --build
   ```

---

## NOTE

Konfigurasi DB **harus sama** dengan **.env** laravel app.

```dotenv
# .env
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=database
DB_USERNAME=username
DB_PASSWORD=secret
DB_ROOT_PASSWORD=secret
```

```dotenv
# source/.env
DB_CONNECTION=mysql
DB_HOST=db #cointainer db
DB_PORT=3306
DB_DATABASE=database
DB_USERNAME=username
DB_PASSWORD=secret
```

Pastikan **nama db** dan **nama user** sama dengan file createdb.sql
```mysql
# compose/mysql/docker-entrypoint-initdb.d/createdb.sql

CREATE DATABASE IF NOT EXISTS 'database';
GRANT ALL ON 'database'.* TO 'username'@'%' ;
```

---
Down dan menghapus Volume:

```sh
docker-compose down -v
```

Inisiasi project:

```sh
sh init-app.sh
```

Update project

```sh
sh update-app.sh
```

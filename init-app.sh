#!/bin/sh
docker-compose run --rm php composer install
docker-compose run --rm node run init
docker-compose run --rm artisan optimize
docker-compose run --rm artisan migrate --seed
docker-compose run --rm node run prod

